package com.rviewer.beertap;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.r2dbc.core.DatabaseClient;
import reactor.core.publisher.Mono;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.PropertyNamingStrategies.SNAKE_CASE;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

@SpringBootApplication
public class RviewerBeerTapApplication {

	public static void main(String[] args) {
		SpringApplication.run(RviewerBeerTapApplication.class, args);
	}

	@Configuration
	public static class CoffeeRegisterModuleConfig {

		@Bean
		public Jackson2ObjectMapperBuilderCustomizer customJackson2ObjectMapperBuilder() {
			return jacksonObjectMapperBuilder -> {
				jacksonObjectMapperBuilder.propertyNamingStrategy(SNAKE_CASE);
				jacksonObjectMapperBuilder.featuresToDisable(FAIL_ON_UNKNOWN_PROPERTIES);
				jacksonObjectMapperBuilder.featuresToDisable(WRITE_DATES_AS_TIMESTAMPS);
				jacksonObjectMapperBuilder.modulesToInstall(new JavaTimeModule());
			};
		}

		@Bean
		public DatabaseInitializer databaseInitializer(DatabaseClient databaseClient) {
			return new DatabaseInitializer(databaseClient);
		}

		private static class DatabaseInitializer {

			private final DatabaseClient databaseClient;

			public DatabaseInitializer(DatabaseClient databaseClient) {
				this.databaseClient = databaseClient;
				createTable().subscribe();
			}

			private Mono<Void> createTable() {
				return databaseClient
						.sql("CREATE TABLE IF NOT EXISTS Dispensers (id SERIAL PRIMARY KEY, dispenser_id VARCHAR(255), flow_volume VARCHAR(255), status VARCHAR(255));" +
							 "CREATE TABLE IF NOT EXISTS Usages (id SERIAL PRIMARY KEY, dispenser_id VARCHAR(255), open_at TIMESTAMP, closed_at TIMESTAMP, usage_spent VARCHAR(255));")
						.fetch()
						.rowsUpdated()
						.then();
			}
		}
	}
}
