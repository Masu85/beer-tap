package com.rviewer.beertap.dispenser.application;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.domain.DispenserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class CalculateDispenserSpentUseCase {

    private final DispenserRepository dispenserRepository;

    public Mono<Dispenser> calculateSpentForDispenserUsages(Dispenser dispenser) {

        dispenser.getDispenserUsage().forEach(usage -> {
            if(usage.getUsageSpent() == null || usage.getClosedAt() == null) {
                usage.calculateSpent(dispenser.getFlowVolume());
            }
        });

        return dispenserRepository.updateDispenser(dispenser);
    }
}
