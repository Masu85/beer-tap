package com.rviewer.beertap.dispenser.application;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.domain.DispenserRepository;
import com.rviewer.beertap.dispenser.domain.Usage;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ChangeDispenserStatusUseCase {

    private final DispenserRepository dispenserRepository;

    public Mono<Dispenser> changeStatus(Dispenser dispenser, Dispenser.DispenserStatus status, Instant currentTime) {

        List<Usage> dispenserUsage = new LinkedList<>(dispenser.getDispenserUsage());

        if (status.equals(Dispenser.DispenserStatus.OPEN) && (dispenserUsage.isEmpty() || dispenserUsage.get(dispenserUsage.size() - 1).getClosedAt() != null)) {
            var newUsage = Usage.builder().openAt(currentTime).build();
            dispenserUsage.add(newUsage);
        }
        else if (status.equals(Dispenser.DispenserStatus.CLOSE) && !dispenserUsage.isEmpty() && dispenserUsage.get(dispenserUsage.size() - 1).getClosedAt() == null) {
            Usage lastUsage = dispenserUsage.get(dispenserUsage.size() - 1);
            lastUsage.setClosedAt(currentTime);
            lastUsage.calculateSpent(dispenser.getFlowVolume());
        }
        else {
            return Mono.error(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unexpected API error"));
        }

        dispenser.setStatus(status);
        dispenser.setDispenserUsage(dispenserUsage);
        return dispenserRepository.updateDispenser(dispenser);
    }
}
