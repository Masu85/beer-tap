package com.rviewer.beertap.dispenser.application;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.domain.DispenserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class CreateDispenserUseCase {
    private final DispenserRepository dispenserRepository;

    public Mono<Dispenser> createDispenser(Dispenser dispenser) {
        return dispenserRepository.createDispenser(dispenser);
    }
}
