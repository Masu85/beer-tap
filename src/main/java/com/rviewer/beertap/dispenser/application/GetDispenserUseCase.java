package com.rviewer.beertap.dispenser.application;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.domain.DispenserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GetDispenserUseCase {

    private final DispenserRepository dispenserRepository;

    public Mono<Dispenser> findById(UUID dispenserId) {
        return dispenserRepository.getbyId(dispenserId);
    }
}
