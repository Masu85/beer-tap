package com.rviewer.beertap.dispenser.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class Dispenser {

    private Integer id;
    private final UUID dispenserId;
    private final BigDecimal flowVolume;
    private DispenserStatus status;
    private List<Usage> dispenserUsage;

    public Dispenser(BigDecimal flowVolume) {
        this.dispenserId = UUID.randomUUID();
        this.flowVolume = flowVolume;
        this.status = DispenserStatus.CLOSE;
        this.dispenserUsage = new LinkedList<>();
    }

    @Getter
    public enum DispenserStatus {
        OPEN("OPEN"),
        CLOSE("CLOSE");

        private final String valorStatus;

        DispenserStatus(String valorStatus) {
            this.valorStatus = valorStatus;
        }

        public static DispenserStatus getByValue(String value) {
            for (DispenserStatus enumValue : DispenserStatus.values()) {
                if (enumValue.getValorStatus().equalsIgnoreCase(value)) {
                    return enumValue;
                }
            }
            return null;
        }
    }
}
