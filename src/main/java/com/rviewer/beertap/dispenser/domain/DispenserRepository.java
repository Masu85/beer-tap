package com.rviewer.beertap.dispenser.domain;

import reactor.core.publisher.Mono;

import java.util.UUID;

public interface DispenserRepository {
    Mono<Dispenser> createDispenser(Dispenser dispenser);

    Mono<Dispenser> updateDispenser(Dispenser dispenser);

    Mono<Dispenser> getbyId(UUID dispenserId);
}
