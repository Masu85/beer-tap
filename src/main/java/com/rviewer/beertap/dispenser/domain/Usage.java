package com.rviewer.beertap.dispenser.domain;

import lombok.*;
import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.Instant;

@Getter
@Setter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class Usage {

    public static final BigDecimal BEER_VALUE = new BigDecimal("12.25");

    private Integer id;
    private final Instant openAt;
    private Instant closedAt;
    private BigDecimal usageSpent;

    public BigDecimal calculateSpent(BigDecimal flowVolume) {

        var lastTimeOpened = closedAt != null ? closedAt : Instant.now();
        var secondsOpened = new BigDecimal(Duration.between(openAt, lastTimeOpened).getSeconds());

        usageSpent = flowVolume.multiply(secondsOpened.multiply(BEER_VALUE));
        return usageSpent.setScale(3, RoundingMode.HALF_UP);
    }
}
