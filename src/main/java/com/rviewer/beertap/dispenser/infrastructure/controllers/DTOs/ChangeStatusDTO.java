package com.rviewer.beertap.dispenser.infrastructure.controllers.DTOs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.Instant;

@Getter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class ChangeStatusDTO {
    private String status;
    private Instant updateAt;
}
