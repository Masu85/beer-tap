package com.rviewer.beertap.dispenser.infrastructure.controllers.DTOs;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Getter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class DispenserDTO {
    private String id;
    private BigDecimal flowVolume;

    public static Dispenser toDispenser(DispenserDTO dispenserDTO) {
        return new Dispenser(dispenserDTO.flowVolume);
    }

    public static DispenserDTO fromDispenser(Dispenser dispenser){
        return new DispenserDTO(dispenser.getDispenserId().toString(), dispenser.getFlowVolume());
    }
}
