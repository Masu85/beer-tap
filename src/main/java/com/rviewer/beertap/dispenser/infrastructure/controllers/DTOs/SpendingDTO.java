package com.rviewer.beertap.dispenser.infrastructure.controllers.DTOs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class SpendingDTO {

    private BigDecimal amount;
    private List<UsageDTO> usages;
}
