package com.rviewer.beertap.dispenser.infrastructure.controllers.DTOs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class UsageDTO {
    private Instant openAt;
    private Instant closedAt;
    private BigDecimal flowVolume;
    private BigDecimal totalSpent;
}
