package com.rviewer.beertap.dispenser.infrastructure.controllers;

import com.rviewer.beertap.dispenser.application.CalculateDispenserSpentUseCase;
import com.rviewer.beertap.dispenser.application.ChangeDispenserStatusUseCase;
import com.rviewer.beertap.dispenser.application.CreateDispenserUseCase;
import com.rviewer.beertap.dispenser.application.GetDispenserUseCase;
import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.infrastructure.controllers.DTOs.ChangeStatusDTO;
import com.rviewer.beertap.dispenser.infrastructure.controllers.DTOs.DispenserDTO;
import com.rviewer.beertap.dispenser.infrastructure.controllers.DTOs.SpendingDTO;
import com.rviewer.beertap.dispenser.infrastructure.controllers.DTOs.UsageDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/dispenser")
@RequiredArgsConstructor
public class DispenserController {

    private final CreateDispenserUseCase createDispenserUseCase;
    private final GetDispenserUseCase getDispenserUseCase;
    private final ChangeDispenserStatusUseCase changeDispenserStatusUseCase;
    private final CalculateDispenserSpentUseCase calculateDispenserSpentUseCase;

    @PostMapping()
    public Mono<DispenserDTO> createDispenser(@RequestBody DispenserDTO dispenserDTO) {
        return createDispenserUseCase.createDispenser(DispenserDTO.toDispenser(dispenserDTO))
                .map(DispenserDTO::fromDispenser)
                .onErrorResume(error -> Mono.error(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unexpected API error")));
    }

    @PutMapping("/{dispenserId}/status")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Mono<Void> changeStatus(@PathVariable UUID dispenserId, @RequestBody ChangeStatusDTO changeStatusDTO) {
        var dispenserFound = getDispenserUseCase.findById(dispenserId);

        return dispenserFound
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Dispenser ID not found")))
                .flatMap(dispenser -> {
                    var statusConverted = Dispenser.DispenserStatus.getByValue(changeStatusDTO.getStatus());

                    if(statusConverted == null || statusConverted.equals(dispenser.getStatus()))
                        return Mono.error(new ResponseStatusException(HttpStatus.CONFLICT, "Status is incorrect or dispenser is already opened/closed"));

                    return changeDispenserStatusUseCase.changeStatus(dispenser, statusConverted, changeStatusDTO.getUpdateAt());
                })
                .then();
    }

    @GetMapping("/{dispenserId}/spending")
    public Mono<SpendingDTO> calculateSpent(@PathVariable UUID dispenserId) {
        var dispenserFound = getDispenserUseCase.findById(dispenserId);

        return dispenserFound
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Dispenser ID not found")))
                .flatMap(calculateDispenserSpentUseCase::calculateSpentForDispenserUsages)
                .map(dispenser -> {
                    AtomicReference<BigDecimal> totalSpent = new AtomicReference<>(BigDecimal.ZERO.setScale(3, RoundingMode.HALF_UP));
                    var usagesDTOList = dispenser.getDispenserUsage().stream()
                            .map(usage -> {
                                totalSpent.set(totalSpent.get().add(usage.getUsageSpent()));

                                return new UsageDTO(usage.getOpenAt(),
                                        usage.getClosedAt(),
                                        dispenser.getFlowVolume(),
                                        usage.getUsageSpent());
                            }).toList();

                    return SpendingDTO.builder().amount(totalSpent.get()).usages(usagesDTOList).build();
                });
    }
}
