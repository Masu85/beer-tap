package com.rviewer.beertap.dispenser.infrastructure.repositories.DAOs;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.domain.Usage;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.UUID;

@Builder
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Table("Dispensers")
public class DispenserDAO {

    @Id
    private Integer id;
    private String dispenserId;
    private String flowVolume;
    private String status;

    public static Dispenser toDispenser(DispenserDAO dispenserDAO, List<UsageDAO> UsagesListDAO) {

        return Dispenser.builder().id(dispenserDAO.getId())
                .dispenserId(UUID.fromString(dispenserDAO.getDispenserId()))
                .flowVolume(new BigDecimal(dispenserDAO.getFlowVolume()))
                .status(Dispenser.DispenserStatus.valueOf(dispenserDAO.getStatus()))
                .dispenserUsage(UsagesListDAO.stream()
                        .map(usageDAO ->
                                Usage.builder().id(usageDAO.getId())
                                        .usageSpent(usageDAO.getUsageSpent() == null ? null : new BigDecimal(usageDAO.getUsageSpent()).setScale(3, RoundingMode.HALF_UP))
                                        .openAt(usageDAO.getOpenAt() == null ? null : usageDAO.getOpenAt())
                                        .closedAt(usageDAO.getClosedAt() == null ? null : usageDAO.getClosedAt())
                                        .build())
                        .toList()).build();
    }

    public static DispenserDAO fromDispenser(Dispenser dispenser) {

        return DispenserDAO.builder().id(dispenser.getId())
                .dispenserId(dispenser.getDispenserId().toString())
                .flowVolume(dispenser.getFlowVolume().toString())
                .status(dispenser.getStatus().toString())
                .build();
    }
}