package com.rviewer.beertap.dispenser.infrastructure.repositories.DAOs;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Table("Usages")
public class UsageDAO {

    @Id
    public Integer id;
    public String dispenserId;
    public Instant openAt;
    public Instant closedAt;
    public String usageSpent;

    public static List<UsageDAO> fromDispenser(Dispenser dispenser) {

        return dispenser.getDispenserUsage().stream().map(usage ->
                        UsageDAO.builder().id(usage.getId())
                                .dispenserId(dispenser.getDispenserId().toString())
                                .usageSpent(usage.getUsageSpent() == null ? null : usage.getUsageSpent().toString())
                                .openAt(usage.getOpenAt() == null ? null : usage.getOpenAt())
                                .closedAt(usage.getClosedAt() == null ? null : usage.getClosedAt())
                                .build())
                .toList();
    }
}