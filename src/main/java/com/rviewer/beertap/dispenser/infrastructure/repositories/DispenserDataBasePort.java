package com.rviewer.beertap.dispenser.infrastructure.repositories;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.domain.DispenserRepository;
import com.rviewer.beertap.dispenser.infrastructure.repositories.DAOs.DispenserDAO;
import com.rviewer.beertap.dispenser.infrastructure.repositories.DAOs.UsageDAO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DispenserDataBasePort implements DispenserRepository {

    private final DispenserPostgresAdapter dispenserPostgresAdapter;
    private final UsagePostgresAdapter usagePostgresAdapter;

    @Override
    public Mono<Dispenser> createDispenser(Dispenser dispenser) {
        var dispenserDAO = DispenserDAO.fromDispenser(dispenser);

        return dispenserPostgresAdapter.save(dispenserDAO)
                .map(savedDispenserDAO -> DispenserDAO.toDispenser(savedDispenserDAO, Collections.emptyList()));
    }

    @Override
    public Mono<Dispenser> updateDispenser(Dispenser dispenser) {
        var dispenserDAO = DispenserDAO.fromDispenser(dispenser);
        var listUsagesDAO = UsageDAO.fromDispenser(dispenser);

        return dispenserPostgresAdapter.save(dispenserDAO).map(savedDispenser ->
            usagePostgresAdapter.saveAll(listUsagesDAO).subscribe()
        )
        .thenReturn(dispenser);
    }

    @Override
    public Mono<Dispenser> getbyId(UUID dispenserId) {
        return dispenserPostgresAdapter.findByDispenserId(dispenserId.toString())
                .flatMap(dispenserDAOFound -> {
                    var usagesDAOListMono = usagePostgresAdapter.findByDispenserId(dispenserDAOFound.getDispenserId()).collectList();
                    return usagesDAOListMono.map(usagesList -> DispenserDAO.toDispenser(dispenserDAOFound, usagesList));
                });
    }
}
