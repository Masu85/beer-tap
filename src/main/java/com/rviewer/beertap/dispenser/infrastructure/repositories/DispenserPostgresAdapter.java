package com.rviewer.beertap.dispenser.infrastructure.repositories;

import com.rviewer.beertap.dispenser.infrastructure.repositories.DAOs.DispenserDAO;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface DispenserPostgresAdapter extends R2dbcRepository<DispenserDAO, String> {
    Mono<DispenserDAO> findByDispenserId(String dispenserId);
}
