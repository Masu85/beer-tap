package com.rviewer.beertap.dispenser.infrastructure.repositories;

import com.rviewer.beertap.dispenser.infrastructure.repositories.DAOs.UsageDAO;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface UsagePostgresAdapter extends R2dbcRepository<UsageDAO, String> {
    Flux<UsageDAO> findByDispenserId(String dispenserId);
}
