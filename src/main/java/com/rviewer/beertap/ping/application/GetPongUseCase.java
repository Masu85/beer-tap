package com.rviewer.beertap.ping.application;

import com.rviewer.beertap.ping.domain.PongResponse;
import com.rviewer.beertap.ping.domain.DatabaseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class GetPongUseCase {

    private final DatabaseRepository databaseRepository;

    public Mono<PongResponse> getPong() {
        return databaseRepository.getConnectionStatus().map(integer ->
            PongResponse.builder().message("pong").number(integer).build()
        );
    }
}
