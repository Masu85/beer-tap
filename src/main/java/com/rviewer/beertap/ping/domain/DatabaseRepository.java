package com.rviewer.beertap.ping.domain;

import reactor.core.publisher.Mono;

public interface DatabaseRepository {

    Mono<Integer> getConnectionStatus();
}
