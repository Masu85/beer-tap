package com.rviewer.beertap.ping.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class PongResponse {
    private String message;
    private int number;
}
