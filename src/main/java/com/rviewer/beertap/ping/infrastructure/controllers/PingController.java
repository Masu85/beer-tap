package com.rviewer.beertap.ping.infrastructure.controllers;

import com.rviewer.beertap.ping.application.GetPongUseCase;
import com.rviewer.beertap.ping.domain.PongResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class PingController {

    private final GetPongUseCase getPongUseCase;

    @GetMapping("/ping")
    public Mono<PongResponse> getPing() {
        return getPongUseCase.getPong();
    }
}
