package com.rviewer.beertap.ping.infrastructure.persistence;

import com.rviewer.beertap.ping.domain.DatabaseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class PostgresPort implements DatabaseRepository {

    private final DatabaseClient databaseR2dbcAdapter;

    public Mono<Integer> getConnectionStatus() {
        return databaseR2dbcAdapter.sql("SELECT 1+1")
                .map((row, rowMetaData) -> row.get(0, Integer.class)).one();
    }

}
