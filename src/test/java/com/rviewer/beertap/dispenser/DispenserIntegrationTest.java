package com.rviewer.beertap.dispenser;

import com.rviewer.beertap.dispenser.infrastructure.controllers.DTOs.DispenserDTO;
import com.rviewer.beertap.dispenser.infrastructure.repositories.DispenserPostgresAdapter;
import com.rviewer.beertap.dispenser.infrastructure.repositories.UsagePostgresAdapter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import java.net.URI;
import java.util.concurrent.atomic.AtomicReference;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureWebTestClient
public class DispenserIntegrationTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private DispenserPostgresAdapter dispenserPostgresAdapter;

    @Autowired
    private UsagePostgresAdapter usagePostgresAdapter;

    @Test
    public void createNewDispenserFeature200() {

        var request = """
                    {
                      "flow_volume": 0.0653
                    }
                """;

        webTestClient.post()
                .uri(URI.create("/dispenser"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(request))
                .exchange()
                .expectStatus().isOk()
                .expectBody(DispenserDTO.class)
                .consumeWith(response -> {
                    var dispenser  = response.getResponseBody();
                    assertThat(dispenser).isNotNull();
                    assertThat(dispenser.getId()).isNotEmpty();
                    assertThat(dispenser.getFlowVolume()).isEqualTo("0.0653");

                    var dispenserDB = dispenserPostgresAdapter.findByDispenserId(dispenser.getId()).block();

                    assertThat(dispenserDB).isNotNull();
                    assertThat(dispenserDB.getDispenserId()).isEqualTo(dispenser.getId());
                    assertThat(dispenserDB.getFlowVolume()).isEqualTo("0.0653");
                });
    }

    @Test
    public void ChangeStateFeature202() {

        var requestCreate = """
                    {
                      "flow_volume": 0.0653
                    }
                """;

        AtomicReference<String> dispenserIdSaved = new AtomicReference<>("");

        webTestClient.post()
                .uri(URI.create("/dispenser"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(requestCreate))
                .exchange()
                .expectStatus().isOk()
                .expectBody(DispenserDTO.class)
                .consumeWith(response -> {
                    var dispenser  = response.getResponseBody();
                    assert dispenser != null;
                    dispenserIdSaved.set(dispenser.getId());
                });

        var requestUpdate = """
                    {
                        "status":"open",
                        "update_at": "2022-01-01T02:01:00Z"
                    }
                """;

        webTestClient.put()
                .uri(URI.create("/dispenser/" + dispenserIdSaved.get() + "/status"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(requestUpdate))
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.ACCEPTED);

        var dispenserDB = dispenserPostgresAdapter.findByDispenserId(dispenserIdSaved.get()).block();

        assertThat(dispenserDB).isNotNull();
        assertThat(dispenserDB.getDispenserId()).isEqualTo(dispenserIdSaved.get());
        assertThat(dispenserDB.getStatus()).isEqualTo("OPEN");

        var usagesListDBMono = usagePostgresAdapter.findByDispenserId(dispenserIdSaved.get()).collectList();
        usagesListDBMono.doOnNext(usagesListDB ->{
           assertThat(usagesListDB.size()).isEqualTo(1);
           assertThat(usagesListDB.get(0).dispenserId).isEqualTo(dispenserIdSaved.get());
           assertThat(usagesListDB.get(0).getOpenAt()).isNotNull();
        });
    }

    @Test
    public void CalculateSpentFeature200() {

        var requestCreate = """
                    {
                      "flow_volume": 0.0653
                    }
                """;

        AtomicReference<String> dispenserIdSaved = new AtomicReference<>("");

        webTestClient.post()
                .uri(URI.create("/dispenser"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(requestCreate))
                .exchange()
                .expectStatus().isOk()
                .expectBody(DispenserDTO.class)
                .consumeWith(response -> {
                    var dispenser  = response.getResponseBody();
                    assert dispenser != null;
                    dispenserIdSaved.set(dispenser.getId());
                });

        var requestUpdateOpen = """
                    {
                        "status":"open",
                        "update_at": "2022-01-01T02:01:00Z"
                    }
                """;

        webTestClient.put()
                .uri(URI.create("/dispenser/" + dispenserIdSaved.get() + "/status"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(requestUpdateOpen))
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.ACCEPTED);

        var requestUpdateClose = """
                    {
                        "status":"close",
                        "update_at": "2022-01-01T02:01:22Z"
                    }
                """;

        webTestClient.put()
                .uri(URI.create("/dispenser/" + dispenserIdSaved.get() + "/status"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(requestUpdateClose))
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.ACCEPTED);


        webTestClient.get()
                .uri(URI.create("/dispenser/" + dispenserIdSaved.get() + "/spending"))
                .exchange()
                .expectStatus().isOk()
                .expectBody().jsonPath("$.amount").isEqualTo("17.598")
                .jsonPath("$.usages").isNotEmpty()
                .jsonPath("$.usages[0].open_at").isNotEmpty()
                .jsonPath("$.usages[0].closed_at").isNotEmpty()
                .jsonPath("$.usages[0].flow_volume").isNotEmpty()
                .jsonPath("$.usages[0].total_spent").isEqualTo("17.598");
    }
}
