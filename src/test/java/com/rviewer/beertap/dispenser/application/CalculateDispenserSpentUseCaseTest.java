package com.rviewer.beertap.dispenser.application;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.domain.DispenserRepository;
import com.rviewer.beertap.dispenser.domain.Usage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.LinkedList;
import java.util.UUID;

import static com.rviewer.beertap.dispenser.domain.Dispenser.builder;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CalculateDispenserSpentUseCaseTest {

    @Mock
    private DispenserRepository dispenserRepository;

    @InjectMocks
    private CalculateDispenserSpentUseCase calculateDispenserSpentUseCase;

    @Test
    public void calculateSpentForDispenserUsagesWithCloseDispenser() {
        var now = Instant.now();
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var dispenser = builder().dispenserId(uuid)
                .status(Dispenser.DispenserStatus.CLOSE)
                .flowVolume(new BigDecimal("0.056"))
                .dispenserUsage(new LinkedList<>()).build();
        var usage = Usage.builder().openAt(now.minusSeconds(22)).closedAt(now).build();

        dispenser.getDispenserUsage().add(usage);

        calculateDispenserSpentUseCase.calculateSpentForDispenserUsages(dispenser);

        dispenser.getDispenserUsage().get(0).setUsageSpent(new BigDecimal("17.518"));

        verify(dispenserRepository).updateDispenser(dispenser);
    }

    @Test
    public void calculateSpentForDispenserUsagesWithOpenDispenser() {
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var dispenser = builder().dispenserId(uuid)
                .status(Dispenser.DispenserStatus.OPEN)
                .flowVolume(new BigDecimal("0.056"))
                .dispenserUsage(new LinkedList<>()).build();
        var usage = Usage.builder().openAt(Instant.now().minusSeconds(22)).build();

        dispenser.getDispenserUsage().add(usage);

        calculateDispenserSpentUseCase.calculateSpentForDispenserUsages(dispenser);

        dispenser.getDispenserUsage().get(0).setUsageSpent(new BigDecimal("17.518"));

        verify(dispenserRepository).updateDispenser(dispenser);
    }

    @Test
    public void calculateSpentForDispenserUsagesWithMultipleUsages() {
        var now = Instant.now();
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var dispenser = builder().dispenserId(uuid)
                .status(Dispenser.DispenserStatus.CLOSE)
                .flowVolume(new BigDecimal("0.056"))
                .dispenserUsage(new LinkedList<>()).build();

        var usage1 = Usage.builder().openAt(now.minusSeconds(44)).closedAt(now.minusSeconds(22)).build();
        var usage2 = Usage.builder().openAt(now.minusSeconds(22)).build();

        dispenser.getDispenserUsage().add(usage1);
        dispenser.getDispenserUsage().add(usage2);

        calculateDispenserSpentUseCase.calculateSpentForDispenserUsages(dispenser);

        dispenser.getDispenserUsage().get(0).setUsageSpent(new BigDecimal("17.518"));
        dispenser.getDispenserUsage().get(1).setUsageSpent(new BigDecimal("17.518"));

        verify(dispenserRepository).updateDispenser(dispenser);
    }
}