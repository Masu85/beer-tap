package com.rviewer.beertap.dispenser.application;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.domain.DispenserRepository;
import com.rviewer.beertap.dispenser.domain.Usage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static com.rviewer.beertap.dispenser.domain.Dispenser.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ChangeDispenserStatusUseCaseTest {

    @Mock
    private DispenserRepository dispenserRepository;

    @InjectMocks
    private ChangeDispenserStatusUseCase changeDispenserStatusUseCase;

    @Test
    public void changeStatusFromCloseToOpenAndCreateUsage(){
        ArgumentCaptor<Dispenser> dispenserCaptor = ArgumentCaptor.forClass(Dispenser.class);

        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var dispenser = builder().dispenserId(uuid)
                .status(Dispenser.DispenserStatus.CLOSE)
                .dispenserUsage(new LinkedList<>()).build();

        when(dispenserRepository.updateDispenser(any())).thenReturn(Mono.empty());

        changeDispenserStatusUseCase.changeStatus(dispenser, Dispenser.DispenserStatus.OPEN, Instant.now());

        verify(dispenserRepository).updateDispenser(dispenserCaptor.capture());
        assertThat(dispenserCaptor.getValue().getStatus()).isEqualTo(Dispenser.DispenserStatus.OPEN);
        assertThat(dispenserCaptor.getValue().getDispenserUsage().get(0).getOpenAt()).isNotNull();
        assertThat(dispenserCaptor.getValue().getDispenserUsage().get(0).getClosedAt()).isNull();
    }

    @Test
    public void changeStatusFromOpenToCloseAndNoAddsNewUsage(){
        ArgumentCaptor<Dispenser> dispenserCaptor = ArgumentCaptor.forClass(Dispenser.class);

        var now = Instant.now();
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");

        var usage = Usage.builder().openAt(now.minusSeconds(22)).build();
        List<Usage> usageList = new LinkedList<>();
        usageList.add(usage);

        var dispenser = builder().dispenserId(uuid)
                .flowVolume(new BigDecimal("0.065"))
                .status(Dispenser.DispenserStatus.OPEN)
                .dispenserUsage(usageList).build();

        when(dispenserRepository.updateDispenser(any())).thenReturn(Mono.empty());

        changeDispenserStatusUseCase.changeStatus(dispenser, Dispenser.DispenserStatus.CLOSE, now);

        verify(dispenserRepository).updateDispenser(dispenserCaptor.capture());
        assertThat(dispenserCaptor.getValue().getStatus()).isEqualTo(Dispenser.DispenserStatus.CLOSE);
        assertThat(dispenserCaptor.getValue().getDispenserUsage().get(0).getOpenAt()).isNotNull();
        assertThat(dispenserCaptor.getValue().getDispenserUsage().get(0).getClosedAt()).isNotNull();
        assertThat(dispenserCaptor.getValue().getDispenserUsage().get(0).getClosedAt()).isEqualTo(now);
        assertThat(dispenserCaptor.getValue().getDispenserUsage().get(0).getUsageSpent()).isNotNull();
    }

    @Test
    public void changeStatusWithStatusError(){
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var dispenser = builder().dispenserId(uuid)
                .status(Dispenser.DispenserStatus.CLOSE)
                .dispenserUsage(new LinkedList<>()).build();

        assertThatThrownBy(() -> changeDispenserStatusUseCase.changeStatus(dispenser, Dispenser.DispenserStatus.CLOSE, Instant.now()).block())
                .isInstanceOf(ResponseStatusException.class)
                .satisfies(ex -> {
                    var exception = (ResponseStatusException) ex;
                    assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
                    assertThat(exception.getReason()).isEqualTo("Unexpected API error");
                });

        verify(dispenserRepository, never()).updateDispenser(any());
    }

}