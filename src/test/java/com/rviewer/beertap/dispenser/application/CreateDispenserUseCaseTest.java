package com.rviewer.beertap.dispenser.application;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.domain.DispenserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CreateDispenserUseCaseTest {
    @Mock
    private DispenserRepository dispenserRepository;
    @InjectMocks
    private CreateDispenserUseCase createDispenserUseCase;

    @Test
    public void createDispenserOk() {

        var dispenser = new Dispenser(new BigDecimal("0.056"));

        when(dispenserRepository.createDispenser(dispenser)).thenReturn(Mono.just(dispenser));
        createDispenserUseCase.createDispenser(dispenser);

        verify(dispenserRepository).createDispenser(dispenser);
    }
}