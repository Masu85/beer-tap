package com.rviewer.beertap.dispenser.application;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.domain.DispenserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GetDispensersUseCaseTest {

    @Mock
    private DispenserRepository dispenserRepository;

    @InjectMocks
    private GetDispenserUseCase getDispensersUseCase;

    @Test
    public void findOneExistingId() {
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var dispenser = Dispenser.builder().dispenserId(uuid).build();

        when(dispenserRepository.getbyId(uuid)).thenReturn(Mono.just(dispenser));

        var dispenserFound = getDispensersUseCase.findById(uuid).block();

        verify(dispenserRepository).getbyId(uuid);

        assertThat(dispenserFound).isNotNull();
        assertThat(dispenserFound.getDispenserId()).isEqualTo(uuid);
    }

    @Test
    public void findOneNoExistingId() {
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");

        when(dispenserRepository.getbyId(uuid)).thenReturn(Mono.empty());

        var dispenserFound = getDispensersUseCase.findById(uuid).block();

        verify(dispenserRepository).getbyId(uuid);

        assertThat(dispenserFound).isNull();
    }
}