package com.rviewer.beertap.dispenser.domain;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class DispenserTest {

    @Test
    public void generateNewDispenserWithAllFields() {
        var flowVolume = new BigDecimal("0.0056");
        var dispenser = new Dispenser(flowVolume);

        assertThat(dispenser.getDispenserId()).isInstanceOf(UUID.class);
        assertThat(dispenser.getFlowVolume()).isSameAs(flowVolume);
        assertThat(dispenser.getStatus()).isEqualTo(Dispenser.DispenserStatus.CLOSE);
        assertThat(dispenser.getDispenserUsage()).isInstanceOf(LinkedList.class);
        assertThat(dispenser.getDispenserUsage()).isEmpty();
    }
}