package com.rviewer.beertap.dispenser.domain;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

class UsageTest {

    @Test
    public void checkRightSpentCalculation() {
        var now = Instant.now();
        var usage = Usage.builder().openAt(now.minusSeconds(22))
                .closedAt(now).build();

        var totalSpent = usage.calculateSpent(new BigDecimal("0.064"));

        var exepectedResult = new BigDecimal("17.248").setScale(3, RoundingMode.HALF_UP);

        assertThat(totalSpent.compareTo(exepectedResult)).isEqualTo(0);
        assertThat(usage.getUsageSpent().compareTo(exepectedResult)).isEqualTo(0);
    }

    @Test
    public void checkRightSpentCalculationWithNiFinishDate() {
        var now = Instant.now();
        var usage = Usage.builder().openAt(now.minusSeconds(22)).build();

        var totalSpent = usage.calculateSpent(new BigDecimal("0.064"));

        var exepectedResult = new BigDecimal("17.248").setScale(3, RoundingMode.HALF_UP);

        assertThat(totalSpent.compareTo(exepectedResult)).isEqualTo(0);
        assertThat(usage.getUsageSpent().compareTo(exepectedResult)).isEqualTo(0);
    }
}