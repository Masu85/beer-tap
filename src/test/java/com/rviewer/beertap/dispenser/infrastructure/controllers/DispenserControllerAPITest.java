package com.rviewer.beertap.dispenser.infrastructure.controllers;

import com.rviewer.beertap.dispenser.application.CalculateDispenserSpentUseCase;
import com.rviewer.beertap.dispenser.application.ChangeDispenserStatusUseCase;
import com.rviewer.beertap.dispenser.application.CreateDispenserUseCase;
import com.rviewer.beertap.dispenser.application.GetDispenserUseCase;
import com.rviewer.beertap.dispenser.domain.Dispenser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.net.URI;
import java.util.LinkedList;
import java.util.UUID;

import static com.rviewer.beertap.dispenser.domain.Dispenser.DispenserStatus.CLOSE;
import static com.rviewer.beertap.dispenser.domain.Dispenser.DispenserStatus.OPEN;
import static com.rviewer.beertap.dispenser.domain.Dispenser.builder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureWebTestClient
class DispenserControllerAPITest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private CreateDispenserUseCase createDispenserUseCaseBean;

    @MockBean
    private ChangeDispenserStatusUseCase changeDispenserStatusUseCase;

    @MockBean
    private GetDispenserUseCase getDispenserUseCase;

    @MockBean
    private CalculateDispenserSpentUseCase calculateDispenserSpentUseCase;

    @Test
    public void createNewDispenserEndpoint() {

        var dispenser = Dispenser.builder().dispenserId(UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb"))
                .flowVolume(new BigDecimal("0.0653"))
                .status(CLOSE)
                .dispenserUsage(new LinkedList<>())
                .build();

        var request = """
                    {
                      "flow_volume": 0.0653
                    }
                """;

        when(createDispenserUseCaseBean.createDispenser(any())).thenReturn(Mono.just(dispenser));

        webTestClient.post()
                .uri(URI.create("/dispenser"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(request))
                .exchange()
                .expectStatus().isOk()
                .expectBody().jsonPath("id").isEqualTo(dispenser.getDispenserId().toString())
                .jsonPath("flow_volume").isEqualTo(dispenser.getFlowVolume());
    }

    @Test
    public void createNewDispenserEndpointWithErrorResponse() {

        var dispenser = Dispenser.builder().dispenserId(UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb"))
                .flowVolume(new BigDecimal("0.0653"))
                .status(CLOSE)
                .dispenserUsage(new LinkedList<>())
                .build();

        var request = """
                    {
                      "flow_volume": 0.0653
                    }
                """;

        when(createDispenserUseCaseBean.createDispenser(any())).thenReturn(Mono.error(new RuntimeException()));

        webTestClient.post()
                .uri(URI.create("/dispenser"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(request))
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody().jsonPath("$.message").isEqualTo("Unexpected API error");
    }

    @Test
    public void changeStatusEndpoint() {

        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var dispenser = Dispenser.builder().dispenserId(uuid)
                .flowVolume(new BigDecimal("0.0653"))
                .status(CLOSE)
                .dispenserUsage(new LinkedList<>())
                .build();

        var dispenserChanged = Dispenser.builder().dispenserId(uuid)
                .flowVolume(new BigDecimal("0.0653"))
                .status(OPEN)
                .dispenserUsage(new LinkedList<>())
                .build();

        var request = """
                    {
                        "status":"open",
                        "update_at": "2022-01-01T02:00:00Z"
                    }
                """;

        when(getDispenserUseCase.findById(uuid)).thenReturn(Mono.just(dispenser));
        when(changeDispenserStatusUseCase.changeStatus(any(), any(), any())).thenReturn(Mono.just(dispenserChanged));

        webTestClient.put()
                .uri(URI.create("/dispenser/" + uuid + "/status"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(request))
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.ACCEPTED);
    }

    @Test
    public void changeStatusEndpointWithError409() {

        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var dispenser = Dispenser.builder().dispenserId(uuid)
                .flowVolume(new BigDecimal("0.0653"))
                .status(CLOSE)
                .dispenserUsage(new LinkedList<>())
                .build();

        var request = """
                    {
                        "status":"close",
                        "update_at": "2022-01-01T02:00:00Z"
                    }
                """;

        when(getDispenserUseCase.findById(uuid)).thenReturn(Mono.just(dispenser));

        webTestClient.put()
                .uri(URI.create("/dispenser/" + uuid + "/status"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(request))
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.CONFLICT);
    }

    @Test
    public void changeStatusEndpointWithError500() {

        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var dispenser = Dispenser.builder().dispenserId(uuid)
                .flowVolume(new BigDecimal("0.0653"))
                .status(CLOSE)
                .dispenserUsage(new LinkedList<>())
                .build();

        var request = """
                    {
                        "status":"open",
                        "update_at": "2022-01-01T02:00:00Z"
                    }
                """;

        when(getDispenserUseCase.findById(uuid)).thenReturn(Mono.just(dispenser));
        when(changeDispenserStatusUseCase.changeStatus(any(), any(), any())).thenReturn(Mono.error(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unexpected API error")));

        webTestClient.put()
                .uri(URI.create("/dispenser/" + uuid + "/status"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(request))
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void calculateSpentEndpoint() {

        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");

        var dispenser = builder().dispenserId(uuid)
                .status(Dispenser.DispenserStatus.CLOSE)
                .dispenserUsage(new LinkedList<>()).build();

        when(getDispenserUseCase.findById(uuid)).thenReturn(Mono.just(dispenser));
        when(calculateDispenserSpentUseCase.calculateSpentForDispenserUsages(dispenser)).thenReturn(Mono.just(dispenser));


        webTestClient.get()
                .uri(URI.create("/dispenser/" + uuid + "/spending"))
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    public void calculateSpentEndpointWithError404() {

        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");

        when(getDispenserUseCase.findById(uuid)).thenReturn(Mono.empty());

        webTestClient.get()
                .uri(URI.create("/dispenser/" + uuid + "/spending"))
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.NOT_FOUND)
                .expectBody().jsonPath("$.message").isEqualTo("Dispenser ID not found");;
    }
}