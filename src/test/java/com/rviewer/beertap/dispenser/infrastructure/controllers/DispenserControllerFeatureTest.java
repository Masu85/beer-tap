package com.rviewer.beertap.dispenser.infrastructure.controllers;

import com.rviewer.beertap.dispenser.application.CalculateDispenserSpentUseCase;
import com.rviewer.beertap.dispenser.application.ChangeDispenserStatusUseCase;
import com.rviewer.beertap.dispenser.application.CreateDispenserUseCase;
import com.rviewer.beertap.dispenser.application.GetDispenserUseCase;
import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.domain.Usage;
import com.rviewer.beertap.dispenser.infrastructure.controllers.DTOs.ChangeStatusDTO;
import com.rviewer.beertap.dispenser.infrastructure.controllers.DTOs.DispenserDTO;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.LinkedList;
import java.util.UUID;

import static com.rviewer.beertap.dispenser.domain.Dispenser.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DispenserControllerFeatureTest {

    @Mock
    private CreateDispenserUseCase createDispenserUseCase;

    @Mock
    private GetDispenserUseCase getDispenserUseCase;

    @Mock
    private ChangeDispenserStatusUseCase changeDispenserStatusUseCase;

    @Mock
    private CalculateDispenserSpentUseCase calculateDispenserSpentUseCase;

    @InjectMocks
    private DispenserController dispenserController;

    @Test
    public void callCreateDispenserUseCaseFromController() {

        var requestDispenserDTO = new DispenserDTO(null, new BigDecimal("0.0653"));
        var responseDispenserDTO = new DispenserDTO("e678cd48-76cc-474c-b611-94dd2df533cb",
                new BigDecimal("0.0653"));

        var dispenser = Dispenser.builder().dispenserId(UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb"))
                .flowVolume(new BigDecimal("0.0653"))
                .status(Dispenser.DispenserStatus.CLOSE)
                .dispenserUsage(new LinkedList<>())
                .build();

        try (MockedStatic<DispenserDTO> staticMethods = Mockito.mockStatic(DispenserDTO.class)) {

            staticMethods.when(() -> DispenserDTO.toDispenser(requestDispenserDTO)).thenReturn(dispenser);
            staticMethods.when(() -> DispenserDTO.fromDispenser(dispenser)).thenReturn(responseDispenserDTO);

            when(createDispenserUseCase.createDispenser(dispenser)).thenReturn(Mono.just(dispenser));
            var resultMono = dispenserController.createDispenser(requestDispenserDTO).block();

            Assertions.assertThat(resultMono).isInstanceOf(DispenserDTO.class);
            Assertions.assertThat(resultMono).isEqualTo(responseDispenserDTO);

            verify(createDispenserUseCase).createDispenser(dispenser);
            staticMethods.verify(() -> DispenserDTO.toDispenser(requestDispenserDTO), times(1));
            staticMethods.verify(() -> DispenserDTO.fromDispenser(dispenser), times(1));
        }
    }

    @Test
    public void callCreateDispenserUseCaseFromControllerWithException() {

        var requestDispenserDTO = new DispenserDTO(null, new BigDecimal("0.0653"));
        var responseDispenserDTO = new DispenserDTO("e678cd48-76cc-474c-b611-94dd2df533cb",
                new BigDecimal("0.0653"));

        var dispenser = Dispenser.builder().dispenserId(UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb"))
                .flowVolume(new BigDecimal("0.0653"))
                .status(Dispenser.DispenserStatus.CLOSE)
                .dispenserUsage(new LinkedList<>())
                .build();

        try (MockedStatic<DispenserDTO> staticMethods = Mockito.mockStatic(DispenserDTO.class)) {

            staticMethods.when(() -> DispenserDTO.toDispenser(requestDispenserDTO)).thenReturn(dispenser);
            staticMethods.when(() -> DispenserDTO.fromDispenser(dispenser)).thenReturn(responseDispenserDTO);

            when(createDispenserUseCase.createDispenser(dispenser)).thenReturn(Mono.error(new RuntimeException()));

            assertThatThrownBy(() -> dispenserController.createDispenser(requestDispenserDTO).block())
                    .isInstanceOf(ResponseStatusException.class)
                    .satisfies(ex -> {
                        var exception = (ResponseStatusException) ex;
                        assertThat(exception.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
                        assertThat(exception.getReason()).isEqualTo("Unexpected API error");

                    });

            verify(createDispenserUseCase).createDispenser(dispenser);
            staticMethods.verify(() -> DispenserDTO.toDispenser(requestDispenserDTO), times(1));
        }
    }

    @Test
    public void callChangeStatusFromController() {
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var now = Instant.now();
        var changeStatusDTO = ChangeStatusDTO.builder().status("open").updateAt(now).build();

        var dispenser = builder().dispenserId(uuid)
                .status(Dispenser.DispenserStatus.CLOSE)
                .dispenserUsage(new LinkedList<>()).build();

        when(getDispenserUseCase.findById(uuid)).thenReturn(Mono.just(dispenser));
        when(changeDispenserStatusUseCase.changeStatus(dispenser, Dispenser.DispenserStatus.OPEN, now)).thenReturn(Mono.empty());

        StepVerifier.create(dispenserController.changeStatus(uuid, changeStatusDTO))
                .expectComplete()
                .verify();

        verify(getDispenserUseCase).findById(uuid);
        verify(changeDispenserStatusUseCase).changeStatus(dispenser, Dispenser.DispenserStatus.OPEN, now);
    }

    @Test
    public void callChangeStatusFromControllerWithNonExistingDispenser() {
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var now = Instant.now();
        var changeStatusDTO = ChangeStatusDTO.builder().status("close").updateAt(now).build();

        when(getDispenserUseCase.findById(uuid)).thenReturn(Mono.empty());

        StepVerifier.create(dispenserController.changeStatus(uuid, changeStatusDTO))
                .expectErrorMatches(throwable -> throwable instanceof ResponseStatusException &&
                        throwable.getMessage().equals("404 NOT_FOUND \"Dispenser ID not found\"") &&
                        ((ResponseStatusException) throwable).getStatusCode().value() == 404
                )
                .verify();

        verify(getDispenserUseCase).findById(uuid);
        verify(changeDispenserStatusUseCase, never()).changeStatus(any(), any(), any());
    }

    @Test
    public void callChangeStatusFromControllerWithCurrentStatus() {
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var now = Instant.now();
        var changeStatusDTO = ChangeStatusDTO.builder().status("close").updateAt(now).build();

        var dispenser = builder().dispenserId(uuid)
                .status(Dispenser.DispenserStatus.CLOSE)
                .dispenserUsage(new LinkedList<>()).build();

        when(getDispenserUseCase.findById(uuid)).thenReturn(Mono.just(dispenser));

        StepVerifier.create(dispenserController.changeStatus(uuid, changeStatusDTO))
                .expectErrorMatches(throwable -> throwable instanceof ResponseStatusException &&
                        throwable.getMessage().equals("409 CONFLICT \"Status is incorrect or dispenser is already opened/closed\"") &&
                        ((ResponseStatusException) throwable).getStatusCode().value() == 409
                )
                .verify();

        verify(getDispenserUseCase).findById(uuid);
        verify(changeDispenserStatusUseCase, never()).changeStatus(dispenser, Dispenser.DispenserStatus.CLOSE, now);
    }

    @Test
    public void callCalculateSpentFromController() {
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var now = Instant.now();

        var dispenser = builder().dispenserId(uuid)
                .status(Dispenser.DispenserStatus.CLOSE)
                .dispenserUsage(new LinkedList<>()).build();
        var usage1 = Usage.builder().openAt(Instant.now().minusSeconds(22))
                .closedAt(now)
                .usageSpent(new BigDecimal("17.518")).build();

        var usage2 = Usage.builder().openAt(Instant.now().minusSeconds(22))
                .closedAt(now)
                .usageSpent(new BigDecimal("17.518")).build();

        dispenser.getDispenserUsage().add(usage1);
        dispenser.getDispenserUsage().add(usage2);

        when(getDispenserUseCase.findById(uuid)).thenReturn(Mono.just(dispenser));
        when(calculateDispenserSpentUseCase.calculateSpentForDispenserUsages(dispenser)).thenReturn(Mono.just(dispenser));

        var spendingDTO = dispenserController.calculateSpent(uuid).block();

        verify(getDispenserUseCase).findById(uuid);
        verify(calculateDispenserSpentUseCase).calculateSpentForDispenserUsages(dispenser);

        assertThat(spendingDTO).isNotNull();
        assertThat(spendingDTO.getAmount().doubleValue()).isEqualTo(35.036);
    }

    @Test
    public void callCalculateSpentFromControllerWithNonExistingDispenser() {
        var uuid = UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb");
        var now = Instant.now();

        when(getDispenserUseCase.findById(uuid)).thenReturn(Mono.empty());

        StepVerifier.create(dispenserController.calculateSpent(uuid))
                .expectErrorMatches(throwable -> throwable instanceof ResponseStatusException &&
                        throwable.getMessage().equals("404 NOT_FOUND \"Dispenser ID not found\"") &&
                        ((ResponseStatusException) throwable).getStatusCode().value() == 404
                )
                .verify();

        verify(getDispenserUseCase).findById(uuid);
        verify(calculateDispenserSpentUseCase, never()).calculateSpentForDispenserUsages(any());
    }
}