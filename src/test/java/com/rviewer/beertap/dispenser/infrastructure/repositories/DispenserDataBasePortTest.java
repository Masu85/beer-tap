package com.rviewer.beertap.dispenser.infrastructure.repositories;

import com.rviewer.beertap.dispenser.domain.Dispenser;
import com.rviewer.beertap.dispenser.infrastructure.repositories.DAOs.DispenserDAO;
import com.rviewer.beertap.dispenser.infrastructure.repositories.DAOs.UsageDAO;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DispenserDataBasePortTest {
    
    @Mock
    private DispenserPostgresAdapter dispenserPostgresAdapter;

    @Mock
    private UsagePostgresAdapter usagePostgresAdapter;

    @InjectMocks
    private DispenserDataBasePort dispenserDataBasePort;
    
    @Test
    public void testCallSaveNewDispenser() {
        
        var dispenser = Dispenser.builder()
                            .dispenserId(UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb"))
                            .flowVolume(new BigDecimal("0.0056").setScale(3, RoundingMode.HALF_UP))
                            .status(Dispenser.DispenserStatus.CLOSE)
                            .dispenserUsage(new LinkedList<>())
                            .build();

        var dispenserDAO = DispenserDAO.builder().dispenserId("e678cd48-76cc-474c-b611-94dd2df533cb").build();

        try (MockedStatic<DispenserDAO> staticMethods = mockStatic(DispenserDAO.class)) {

            staticMethods.when(() -> DispenserDAO.toDispenser(dispenserDAO, Collections.emptyList())).thenReturn(dispenser);
            staticMethods.when(() -> DispenserDAO.fromDispenser(dispenser)).thenReturn(dispenserDAO);

            when(dispenserPostgresAdapter.save(dispenserDAO)).thenReturn(Mono.just(dispenserDAO));

            var dispenserSaved = dispenserDataBasePort.createDispenser(dispenser).block();

            Assertions.assertThat(dispenserSaved).isInstanceOf(Dispenser.class);
            Assertions.assertThat(dispenserSaved).isEqualTo(dispenser);

            verify(dispenserPostgresAdapter).save(dispenserDAO);

            staticMethods.verify(() -> DispenserDAO.toDispenser(dispenserDAO, Collections.emptyList()), times(1));
            staticMethods.verify(() -> DispenserDAO.fromDispenser(dispenser), times(1));
        }
    }

    @Test
    public void testCallSaveUpdateDispenser() {

        var dispenser = Dispenser.builder()
                .dispenserId(UUID.fromString("e678cd48-76cc-474c-b611-94dd2df533cb"))
                .flowVolume(new BigDecimal("0.0056").setScale(3, RoundingMode.HALF_UP))
                .status(Dispenser.DispenserStatus.OPEN)
                .dispenserUsage(new LinkedList<>())
                .build();

        var dispenserDAO = DispenserDAO.builder().dispenserId("e678cd48-76cc-474c-b611-94dd2df533cb")
                .status("OPEN").build();

        try (MockedStatic<DispenserDAO> staticMethods = mockStatic(DispenserDAO.class)) {

            staticMethods.when(() -> DispenserDAO.toDispenser(dispenserDAO, Collections.emptyList())).thenReturn(dispenser);
            staticMethods.when(() -> DispenserDAO.fromDispenser(dispenser)).thenReturn(dispenserDAO);

            when(dispenserPostgresAdapter.save(dispenserDAO)).thenReturn(Mono.just(dispenserDAO));
            when(usagePostgresAdapter.saveAll(any(List.class))).thenReturn(Flux.empty());

            var dispenserSaved = dispenserDataBasePort.updateDispenser(dispenser).block();

            Assertions.assertThat(dispenserSaved).isInstanceOf(Dispenser.class);
            Assertions.assertThat(dispenserSaved).isEqualTo(dispenser);

            verify(dispenserPostgresAdapter).save(dispenserDAO);
            verify(usagePostgresAdapter).saveAll(any(List.class));

            staticMethods.verify(() -> DispenserDAO.fromDispenser(dispenser), times(1));
        }
    }
}