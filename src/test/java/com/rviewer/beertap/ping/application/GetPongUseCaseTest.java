package com.rviewer.beertap.ping.application;

import com.rviewer.beertap.ping.domain.DatabaseRepository;
import com.rviewer.beertap.ping.domain.PongResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GetPongUseCaseTest {

    @Mock
    private DatabaseRepository databaseRepository;

    @InjectMocks
    private GetPongUseCase getPongUseCase;

    @Test
    public void getPongTest(){

        when(databaseRepository.getConnectionStatus()).thenReturn(Mono.just(2));
        getPongUseCase.getPong();
        verify(databaseRepository).getConnectionStatus();
    }

}