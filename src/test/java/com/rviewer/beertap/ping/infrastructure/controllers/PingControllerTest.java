package com.rviewer.beertap.ping.infrastructure.controllers;

import com.rviewer.beertap.ping.application.GetPongUseCase;
import com.rviewer.beertap.ping.domain.PongResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.net.URI;

import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureWebTestClient
public class PingControllerTest {

    @MockBean
    private GetPongUseCase getPongUseCase;

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void getPing_success() throws Exception {

        var pongResponse = PongResponse.builder().message("pong").number(2).build();

        when(getPongUseCase.getPong()).thenReturn(Mono.just(pongResponse));

        webTestClient.get()
                .uri(URI.create("/ping"))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("message").isEqualTo("pong");
    }
}
