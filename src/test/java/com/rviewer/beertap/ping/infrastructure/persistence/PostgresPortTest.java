package com.rviewer.beertap.ping.infrastructure.persistence;

import com.rviewer.beertap.ping.infrastructure.persistence.PostgresPort;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.DatabaseClient.GenericExecuteSpec;
import org.springframework.r2dbc.core.FetchSpec;
import reactor.core.publisher.Mono;

import java.util.function.BiFunction;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PostgresPortTest {

    @Mock
    private DatabaseClient databaseClient;

    @InjectMocks
    private PostgresPort postgresPort;

    @Test
    public void getConnectionStatus_shouldReturn2() {

        var fetchSpect = mock(FetchSpec.class);
        var genericExecuteSpec = mock(GenericExecuteSpec.class);

        when(databaseClient.sql("SELECT 1+1")).thenReturn(genericExecuteSpec);
        when(genericExecuteSpec.map(Mockito.<BiFunction<Row, RowMetadata, Integer>>any())).thenReturn(fetchSpect);
        when(fetchSpect.one()).thenReturn(Mono.just(2));

        assertThat(postgresPort.getConnectionStatus().block()).isEqualTo(2);
    }
}
